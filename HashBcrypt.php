<?php

/**
 * This File is part of the Stream\Cryptography package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */
namespace Stream\Cryptography;
use Stream\Cryptography\Interfaces\Hash as InterfaceHash;

/**
 * Class Hashbcrypt
 * @author
 */
class HashBcrypt implements InterfaceHash
{
    /**
     * defult
     *
     * @var string
     */
    private static $default = ['cost' => 7];

    /**
     * hash
     *
     * @param mixed $password
     * @param array $options
     */
    public function hash($password, array $options = null)
    {
        $options = is_array($options) ? array_merge(static::$default, $options) : static::$default;
        return password_hash($password, PASSWORD_BCRYPT, $options);
    }

    /**
     * check
     *
     * @param mixed $password
     * @param mixed $hash
     */
    public function check($password, $hash)
    {
        return password_verify($password, $hash);
    }
}

