<?php

/**
 * This File is part of the Stream\Cryptography package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Cryptography\Interfaces;

interface Hash
{
    /**
     * create hash from string
     *
     * @param string $string  string to be hased
     * @param array  $options optional configuration
     */
    public function hash($string, array $options = null);

    /**
     * check an input value agains a hash
     *
     * @param string $string value to be compared agains hash
     * @param string $hash   hash to be compared against string
     */
    public function check($string, $hash);
}
