<?php

/**
 * This File is part of the Stream\Cryptography package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Library\Tests\Cryptography;
use Stream\Cryptography\HashBcrypt;

/**
 * Class HashBcryptTest
 * @author
 */
class HashBcryptTest extends TestCases
{

    protected function setUp()
    {
        $this->hash = new HashBcrypt();
    }

}
