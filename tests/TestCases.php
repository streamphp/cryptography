<?php

/**
 * This File is part of the Stream\Cryptography package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Library\Tests\Cryptography;

/**
 * Class Hashbcrypttest
 * @author
 */
class TestCases extends \PHPUnit_Framework_TestCase
{
    protected $hash;

    /**
     * @test
     * @covers Hash#hash()
     * @covers Hash#check()
     */
    public function testHashCreateAndValidate()
    {
        $hash = $this->hash->hash('bragging');
        $this->assertTrue($this->hash->check('bragging', $hash));
        $this->assertFalse($this->hash->check('brAgging', $hash));
    }
}
